################################# ENTRY POINT #################################

  ZERO
LOOP:  #backward jumps are overshooting, see JUMP 
  GET_PTR PTR         # Read character pointed by PTR
  
  STORE CHAR          # Save it in variable CHAR

  JEZ CHAR, END   #  CHAR equals zero? If yes, end.
  
  PUTCHAR CHAR        # Otherwise, put character

  ADD PTR, PTR, ONE   # Increase string pointer
  JUMP LOOP           # Repeat!
  
END:
  ZERO                # We reach this place from a forward jump
  EXIT                # Exit VM

################################ PROGRAM DATA #################################
PTR:
  rssb HELLO # Address of first char of the string

CHAR:
  rssb 0
  
HELLO:
  rssb 'H'
  rssb 'e'
  rssb 'l'
  rssb 'l'
  rssb 'o'
  rssb ' '
  rssb 'w'
  rssb 'o'
  rssb 'r'
  rssb 'l'
  rssb 'd'
  rssb 0x0a
  rssb 0
  
############################## MACRO DEFINITIONS ##############################
.macro ZERO
    rssb $a
.end

.macro GET ADDR
    ZERO
    rssb ADDR #gets noninverted value
.end

.macro CLEAR ADDR
    ZERO      # $a = 0. No skip
    rssb ADDR # *addr = *addr - 0 = *addr, $a = *addr. No skip
    rssb ADDR # *addr = *addr - *addr = 0, $a = 0. No skip
.end
  
.macro STORE ADDR
  rssb TMP      # Save $a (-a)
  rssb $a       # Only executed if $a was 0 (and does nothing)
  CLEAR ADDR    # *ADDR = $a = 0x0
  rssb TMP      # $a = -$old_a
  rssb ADDR     # *ADDR = $old_a
  ZERO          # previous instr might skip
  CLEAR TMP     # Leave TMP cleared. $a IS NOW 0
.end

.macro MOVE DEST ORIGIN
  GET ORIGIN
  STORE DEST
.end

.macro PUTCHAR ADDR
  ZERO             # $a = 0
  rssb ADDR        # $a = *addr
  INVERT
  rssb $out        # 0 - $a = 0 - (-*addr) = *addr
  rssb $a          # Skipped if *addr != 0
.end

.macro EXIT
  ZERO             # Clear accumulator
  rssb $ip
.end

.macro INVERT
  rssb $0 
  rssb $a # skipped if nonzero. Does nothing if zero. Effectively NOP to counteract rssb skip behavior. 
.end

.macro ADD RES OP1 OP2
  MOVE RES, OP1   # Move OP1 to RES. $a = 0
  rssb OP2        # Put OP2 in $a. OP2 untouched
  INVERT
  rssb RES        # RES: OP1 - (-OP2) = OP1 + OP2
  rssb $a         ############ Potentially skipped ###############
.end

.macro SUB RES OP1 OP2
  MOVE RES, OP1   # Move OP1 to RES. $a = 0
  rssb OP2        # Put OP2 in $a. OP2 untouched
  rssb RES        # RES: OP1 - OP2
  rssb $a         ############# Potentially skipped #############
.end

.macro JUMP LABEL
  GET LOOPOFF
  rssb $ip # ip = ip - ( - offset) = ip + (Label - ip) Produces skip if jumping forward, but not for backward jumps!
LOOPOFF:
  rssb %-LABEL   # Distance from last LABEL
.end

.macro SKIPNZ A #skips next instr if nz. Inverts
  GET A
  rssb $0               # 0-A
.end

.macro JEZ A LABEL # we will consider A a superpos of 2 packages AZ (0) and ANZ (!0)
  MOVE SKIP2 A
  SKIPNZ A
  rssb ONE # if A was zero, it is now 1. This instr is skipped if A!=0
  INVERT # AZ variant is now -1 ANZ variant is back to positive
  rssb SKIP2 # SKIP2 (A backup) for AZ is 0 - so AZ inverts to 1 and skips next instr. ANZ becomes zero!
  rssb FIVE # ANZ becomes 5. AZ skips this instr
  INVERT               # Make it negative
  rssb $ip             # Jump ahead 1 (2 because of skip!) or 5 instr (6 because of skip). ALWAYS produces borrow.
  ZERO                 # Spacer. Never executed. (you can't jump only 1. Skip is in play, so 2)
  JUMP LABEL           # JUMP is 4 instructions long 
  ZERO
  
.end

.macro GET_PTR ADDR
  MOVE DO_GET, ADDR    # Alter instruction to retrieve given value
  ZERO                 # Clear accumulator
DO_GET:
  rssb 0               # Modified by pervious MOVE
.end


############################## TEMPORARY STORAGE ##############################
TMP:
  rssb 0

COPY:
  rssb 0

COPY2:
  rssb 0
  
SKIP:
  rssb 0

SKIP2:
  rssb 0

SKIP3:
  rssb 0
  
################################### .rodata ###################################
ONE: 
  rssb 1
FIVE:
  rssb 5
  