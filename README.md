## RSSB-CPU

Demonstration:
https://peertube.social/videos/watch/ca7ec6d5-e259-477c-b82e-278071f3a96d

A small One Instruction Set Computer (OISC) using the single command "rssb" (Reverse Substract and Skip if Borrow).

Made in Helmut Neeman's Digital simulator  
https://github.com/hneemann/Digital

Example program assembled with modified BatchDrake's rssb assembler+vm:

original  
https://github.com/BatchDrake/rssb  

modified  
https://gitlab.com/Houkime/rssb-assembler-w-macros

Example program is a bit optimized and adapted helloworld_macros from BatchDrake.

Design can be theoretically exported to Verilog or VHDL (for FPGA tests or ASIC design), though it is not the focus at the moment.

### Technical Description:

* 8-bit (easily scalable, no problems here)
* 2 RAM cycles per instruction:
  * current instruction body (targeted by program counter) is stored in a separate register for use in second cycle
  * calculations and all memory writing (memory/register targeted by pointer is swapped with a new value and regs are updated in parallel)
* Memory-mapped IO (see below)
* 5 address-accessible registers (async from main RAM, hence 2 cycles and not 3+):
  * 0: programm counter (PC)
    * auto increments by 1 each instruction IF NOT REWRITTEN BY THAT INSTRUCTION
    * if substraction produces borrow, incremented 1 time more EVEN IF REWRITTEN BY OPERATION THAT PRODUCED THAT BORROW
  * 1: accumulator (ACC)
    * is subtracted from memory (including itself) and rewritten by result
  * 2: nil (NIL)
    * zero constant
  * 3: Input value (I)
    * externally accessible (though no _waiting_ for input yet)
  * 4: Output value (O)
    * Can be used to write values to the terminal if terminal refreshes on this register's write rising edge with delay (implemented soon!)
* one inaddressable register:
  * pointer: carries a value (pointer) retrieved from pc in first cycle for usage in second cycle
  
### Usage:

* git clone https://gitlab.com/Houkime/rssb-cpu
* using Digital, open rssb-digital
* Edit > circuit-specific settings > Advanced
* Select as "program file" InitRAM.hex (helloworld by default) or one of .hex examples
* Now you can just press SPACE and watch the thing (hopefully) working

## TODO list

  * Run helloword! [CURRENT]:
    * adapt some helloworld to writing into output instead of just memory [CURRENT]
    * find/make a reasonable assembler [TIED WITH PREVIOUS]
    * compile
    * test